<?php

namespace backend\modules\Appleal\controllers;

use yii\web\Controller;

/**
 * Default controller for the `appleal` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
