<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\repayment\models\LoanRepaymentItem */

$this->title = 'Update Loan Repayment Item: ' . $model->loan_repayment_item_id;
$this->params['breadcrumbs'][] = ['label' => 'Loan Repayment Items', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->loan_repayment_item_id, 'url' => ['view', 'id' => $model->loan_repayment_item_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loan-repayment-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
