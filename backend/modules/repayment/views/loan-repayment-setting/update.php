<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\repayment\models\LoanRepaymentSetting */

$this->title = 'Update Loan Repayment Setting: ' . $model->loan_repayment_setting_id;
$this->params['breadcrumbs'][] = ['label' => 'Loan Repayment Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->loan_repayment_setting_id, 'url' => ['view', 'id' => $model->loan_repayment_setting_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="loan-repayment-setting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
