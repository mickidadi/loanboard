<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\repayment\models\PayMethod */

$this->title = 'Update Pay Method: ' . $model->pay_method_id;
$this->params['breadcrumbs'][] = ['label' => 'Pay Methods', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pay_method_id, 'url' => ['view', 'id' => $model->pay_method_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pay-method-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
