<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\modules\repayment\models\BankAccount */

$this->title = 'Update Bank Account: ' . $model->bank_account_id;
$this->params['breadcrumbs'][] = ['label' => 'Bank Accounts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->bank_account_id, 'url' => ['view', 'id' => $model->bank_account_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bank-account-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
