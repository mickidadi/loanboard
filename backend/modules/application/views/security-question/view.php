<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\application\models\SecurityQuestion */

$this->title = $model->security_question_id;
$this->params['breadcrumbs'][] = ['label' => 'Security Questions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="security-question-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->security_question_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->security_question_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'security_question_id',
            'security_question',
        ],
    ]) ?>

</div>
