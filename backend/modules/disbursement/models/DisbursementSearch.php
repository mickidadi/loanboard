<?php

namespace backend\modules\disbursement\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\modules\disbursement\models\Disbursement;

/**
 * DisbursementSearch represents the model behind the search form about `backend\modules\disbursement\models\Disbursement`.
 */
class DisbursementSearch extends Disbursement
{
    /**
     * @inheritdoc
     */
      public $firstName;
      public $lastName;
      public $f4indexno;
    public function rules()
    {
        return [
            [['disbursement_id', 'disbursement_batch_id', 'application_id', 'programme_id', 'loan_item_id', 'status', 'created_by'], 'integer'],
            [['disbursed_amount'], 'number'],
            [['created_at','f4indexno','firstName','lastName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$id)
    {
        $query = Disbursement::find()->where(['disbursement_batch_id' => $id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->joinwith("application");
         $query->joinwith(["application","application.applicant"]);
         $query->joinwith(["application","application.applicant"]);
        $query->joinwith(["application","application.applicant.user"]);
        $query->andFilterWhere([
            'disbursement_id' => $this->disbursement_id,
            'disbursement_batch_id' => $id,
            'application_id' => $this->application_id,
            'disbursement.programme_id' => $this->programme_id,
            'loan_item_id' => $this->loan_item_id,
            'disbursed_amount' => $this->disbursed_amount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
        ]);
      $query->andFilterWhere(['like', 'user.firstname', $this->firstName])
         ->andFilterWhere(['like', 'user.surname', $this->lastName])
        ->andFilterWhere(['like', 'applicant.f4indexno', $this->f4indexno]);
       
        return $dataProvider;
    }
}
