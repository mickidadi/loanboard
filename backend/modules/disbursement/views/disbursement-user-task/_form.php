<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementUserTask */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-user-task-form">

    <?php $form = ActiveForm::begin(); ?>
       <div class="profile-user-info profile-user-info-striped">
           <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Loan Board Staff:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
  <?=$form->field($model, 'user_id')->label(FALSE)->widget(Select2::classname(), [
    'data' =>ArrayHelper::map(common\models\User::find()->where("login_type=5")->all(),'user_id',function ($user, $defaultValue)
       {
        return $user->firstname.' '.$user->surname.' '.$user->middlename;
      }),
   // 'language' => 'de',
    'options' => ['multiple' => false,'placeholder' => 'Select Loan Board Staff...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
]);?>  
                            </div>
                </div>
            </div>
     <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Disbursement Structure:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'disbursement_structure_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\modules\disbursement\models\DisbursementStructure::find()->all(),'disbursement_structure_id','structure_name'),
                                [
                                'prompt'=>'[--Select Disbursement Structure--]',
                                ]
                              ) ?>
         </div>
                </div>
            </div>
        </div>
        <div class="space10"></div>
        <div class="col-sm-12">
            <div class="form-group button-wrapper">
 
 <?= Html::a('Cancel', ['index'], ['class' => 'btn btn-primary pull-left']) ?>
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>
 
    </div>
<div class="space10"></div>