<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementTaskAssignment */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="disbursement-task-assignment-form">

    <?php $form = ActiveForm::begin(); ?>
            <div class="profile-user-info profile-user-info-striped">
        
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Disbursement Structure:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'disbursement_structure_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\modules\disbursement\models\DisbursementStructure::find()->all(),'disbursement_structure_id','structure_name'),
                                [
                                'prompt'=>'[--Select Disbursement Structure--]',
                                ]
                              ) ?>
    </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Disbursement Task:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
     <?= $form->field($model, 'disbursement_task_id')->label(false)->dropDownList(
                           ArrayHelper::map(backend\modules\disbursement\models\DisbursementTask::find()->all(),'disbursement_task_id','task_name'),
                                [
                                'prompt'=>'[--Select Disbursement Task--]',
                                ]
                              ) ?>
            </div>
                </div>
            </div>
        </div>
        <div class="space10"></div>
        <div class="col-sm-12">
            <div class="form-group button-wrapper">
 <?= $form->field($model, 'disbursement_schedule_id')->label(false)->hiddenInput(["value"=>$model->isNewRecord ?$disbursement_schedule_id:$model->disbursement_schedule_id]) ?>
 <?= Html::a('Cancel', ['index','id'=>$model->isNewRecord ?$disbursement_schedule_id:$model->disbursement_schedule_id], ['class' => 'btn btn-primary']) ?>
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success pull-right' : 'btn btn-primary pull-right']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>
 
    </div>
<div class="space10"></div>