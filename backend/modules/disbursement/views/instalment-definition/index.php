<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\disbursement\models\InstalmentDefinitionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'List of Instalment';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instalment-definition-index">
   <div class="panel panel-info">
                        <div class="panel-heading">
                      <?= Html::encode($this->title) ?>
                        </div>
                        <div class="panel-body">
    <p>
        <?= Html::a('Create Instalment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'instalment_definition_id',
            'instalment',
            'instalment_desc',
            'is_active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
   </div>
</div>