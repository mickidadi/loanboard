<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\DisbursementSchedule */
/* @var $form yii\widgets\ActiveForm */
?>
 <script type="text/javascript">

 /********************  
  * Id can be hide or show
  * 
	*onload functions
	********************/
	window.onload = start;
	function start () {
		//alert("mickidadi");
		body();
		 
	}
     function body() { 
 
   //disbursementbatch-disburse
     updateStatus();
        }
    function updateStatus(){
      var category= document.getElementById('disbursementschedule-operator_name').value;
                      //alert(category);
             if(category=="Greater than"){
             var cat1= document.getElementById('disbursementscheduleId').style.display = 'none';         
                  }
                else{
          var cat1= document.getElementById('disbursementscheduleId').style.display = '';          
                }
    }
 </script>
<div class="disbursement-schedule-form">
 
    <?php $form = ActiveForm::begin(); ?>
   <div class="profile-user-info profile-user-info-striped">
             <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Operator Name :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
                        <?=
                        $form->field($model, 'operator_name')->label(false)->dropDownList(
                                ['Between'=>"Between",'Greater than'=>"Greater than"], [
                              'prompt' => '[--Select Operator--]',
                              'onchange'=>'updateStatus()'
                                ]
                        )
                        ?>
     </div>
                </div>
            </div>
              <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Min Amount :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'from_amount')->label(false)->textInput(['maxlength' => true]) ?>
       </div>
                </div>
            </div>
            <div class="profile-info-row" id="disbursementscheduleId">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Max Amount :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
    <?= $form->field($model, 'to_amount')->label(false)->textInput(['maxlength' => true]) ?>
            </div>
                </div>
            </div>
        </div>
</div>
        <div class="space10"></div>
        <div class="col-sm-12">
            <div class="form-group button-wrapper">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>

<div class="space10"></div>