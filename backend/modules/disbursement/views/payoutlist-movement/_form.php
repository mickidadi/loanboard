<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model backend\modules\disbursement\models\PayoutlistMovement */
/* @var $form yii\widgets\ActiveForm */
 $tzsql= Yii::$app->db->createCommand("SELECT SUM(disbursed_amount) as amount FROM `disbursement` di WHERE di.disbursement_batch_id='{$disbursementId}'")->queryAll();           
   // $modelamount=  \backend\modules\disbursement\models\Disbursement::find()->select('SUM(disbursed_amount)')->where(['disbursement_batch_id'=>$disbursementId])->all(); 
  //  print_r($tzsql);
     $amountlimit=0;
     //echo $disbursementId;
     //echo $sqlall="SELECT group_concat(du.`disbursement_structure_id`) as groupdata FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.`disbursement_structure_id` AND operator_name='Between' AND from_amount<'{$amountlimit}' AND to_amount<='{$amountlimit}'";
      
    foreach($tzsql as $tzrow);
       $amountlimit=$tzrow["amount"];
        $sqlall="SELECT Max(du.`disbursement_structure_id`) as maxlevel FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.`disbursement_structure_id` AND operator_name='Between' AND from_amount<'{$amountlimit}' AND to_amount<='{$amountlimit}'";
       $modelp= Yii::$app->db->createCommand($sqlall)->queryAll();         
       foreach ($modelp as $rows);
        $structureId=$rows["maxlevel"];
    //SELECT * FROM `disbursement_task_assignment` dt,disbursement_task_definition dd,disbursement_schedule ds , disbursement_user_structure du WHERE dd.disbursement_task_id=dt.`disbursement_task_id` AND ds.disbursement_schedule_id=dt.`disbursement_schedule_id` AND du.`disbursement_structure_id`=dt.disbursement_structure_id
     $sqlmax="SELECT group_concat(user_id) as listuser FROM disbursement_user_structure WHERE disbursement_structure_id='{$structureId}'";
      $modelmax= Yii::$app->db->createCommand($sqlmax)->queryAll();  
    foreach($modelmax as $rowmax);
      $alluserId=$rowmax["listuser"];
       ?>

<div class="payoutlist-movement-form">

    <?php $form = ActiveForm::begin(); ?>
  <div class="col-sm-8">
       <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
        <div class="profile-info-name">
          <label class="control-label" for="email">Officer Name:</label>
        </div>
        <div class="profile-info-value">
    <div class="col-sm-12">
    <?= $form->field($model, 'disbursements_batch_id')->label(false)->hiddenInput(["value"=>$disbursementId]) ?>
    <?= $form->field($model, 'from_officer')->label(false)->hiddenInput(['value'=>\yii::$app->user->identity->user_id]) ?>
    <?=$form->field($model, 'to_officer')->label(false)->widget(DepDrop::classname(), [
                            'data' => ArrayHelper::map(\common\models\User::find()->where("login_type=5 AND  user_id IN($alluserId)")->all(), 'user_id', 'firstname'),
                           'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => [''],
                                'url' => Url::to(['/disbursement/disbursement-dependent/loan-item']),
                                'loadingText' => 'Loading child level 1 ...',
                            ]
                        ]);
                        ?>
        </div>
            </div>
             <div class="profile-info-row">
        <div class="profile-info-name">
          <label class="control-label" for="email">Comment:</label>
        </div>
        <div class="profile-info-value">
    <div class="col-sm-12">
    <?= $form->field($model, 'comment')->label(false)->textArea(['row' => 4]) ?>
    <?= $form->field($model, 'date_out')->label(false)->hiddenInput(["value"=>date("Y-m-d")]) ?>
 </div>
        </div>
            </div>
          
       </div>
       
  </div>
    
  
     <div class="col-sm-12">
          <div class="space10"></div>
    <div class="form-group button-wrapper">
         <div class="space10"></div>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
     </div>
    <?php ActiveForm::end(); ?>

</div>
   <div class="space10"></div>
