 
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model backend\models\AcademicYear */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="academic-year-form">
<?php $form = ActiveForm::begin(); ?>
    <div class="col-sm-8">
        <div class="profile-user-info profile-user-info-striped">
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Academic Year:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
                        <?=
                        $form->field($model, 'academic_year_id')->label(false)->dropDownList(
                                ArrayHelper::map(backend\models\AcademicYear::find()->all(), 'academic_year_id', 'academic_year'), [
                            'prompt' => '[--Select Academic Year--]',
                                ]
                        )
                        ?>
                    </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Instalment :</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
                        <?=
                        $form->field($model, 'instalment_definition_id')->label(false)->dropDownList(
                                ArrayHelper::map(\backend\modules\disbursement\models\InstalmentDefinition::findAll(["is_active" => 1]), 'instalment_definition_id', 'instalment'), [
                            'prompt' => '[--Select Instalment--]',
                                ]
                        )
                        ?>
                    </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Loan Item:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
                        <?=
                        $form->field($model, 'loan_item_id')->label(false)->widget(DepDrop::classname(), [
                            'data' => ArrayHelper::map(backend\modules\allocation\models\LoanItem::findAll(["is_active" => 1]), 'loan_item_id', 'item_name'),
                            'options' => ['placeholder' => 'Select Project Output', 'id' => 'cat-id'],
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => [''],
                                'url' => Url::to(['/disbursement/disbursement-dependent/loan-item']),
                                'loadingText' => 'Loading child level 1 ...',
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
            <div class="profile-info-row">
                <div class="profile-info-name">
                    <label class="control-label" for="email">Associated Loan Item:</label>
                </div>
                <div class="profile-info-value">
                    <div class="col-sm-12">
                        <?=
                        $form->field($model, 'associated_loan_item_id')->label(FALSE)->widget(DepDrop::classname(), [
                            'data' => ArrayHelper::map(backend\modules\allocation\models\LoanItem::findAll(["is_active" => 1,]), 'loan_item_id', 'item_name'),
                            'options' => ['placeholder' => 'Select ...', 'id' => 'cat-id1'],
                            'type' => DepDrop::TYPE_SELECT2,
                            'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                            'pluginOptions' => [
                                'depends' => ['cat-id'],
                                'url' => Url::to(['/disbursement/disbursement-dependent/loan-item']),
                                'loadingText' => 'Loading child level 2 ...',
                            ]
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="space10"></div>
        <div class="col-sm-12">
            <div class="form-group button-wrapper">
<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>
<?php ActiveForm::end(); ?>

    </div>
</div>
<div class="space10"></div>