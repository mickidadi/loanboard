<?php

namespace backend\modules\allocation\models;

use Yii;

/**
 * This is the model class for table "loan_item".
 *
 * @property integer $loan_item_id
 * @property string $item_name
 * @property string $item_code
 * @property double $day_rate_amount
 * @property integer $is_active
 *
 * @property Allocation[] $allocations
 * @property AllocationSetting[] $allocationSettings
 * @property Disbursement[] $disbursements
 * @property DisbursementSetting[] $disbursementSettings
 * @property DisbursementSetting2[] $disbursementSetting2s
 * @property DisbursementSetting2[] $disbursementSetting2s0
 * @property InstitutionPaymentRequest[] $institutionPaymentRequests
 * @property InstitutionPaymentRequestDetail[] $institutionPaymentRequestDetails
 * @property ProgrammeFee[] $programmeFees
 */
class LoanItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loan_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'item_code'], 'required'],
            [['day_rate_amount'], 'number'],
            [['is_active'], 'integer'],
            [['item_name'], 'string', 'max' => 45],
            [['item_code'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'loan_item_id' => 'Loan Item ID',
            'item_name' => 'Item Name',
            'item_code' => 'Item Code',
            'day_rate_amount' => 'Day Rate Amount',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocations()
    {
        return $this->hasMany(Allocation::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllocationSettings()
    {
        return $this->hasMany(AllocationSetting::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursements()
    {
        return $this->hasMany(Disbursement::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursementSettings()
    {
        return $this->hasMany(DisbursementSetting::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursementSetting2s()
    {
        return $this->hasMany(DisbursementSetting2::className(), ['associated_loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDisbursementSetting2s0()
    {
        return $this->hasMany(DisbursementSetting2::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionPaymentRequests()
    {
        return $this->hasMany(InstitutionPaymentRequest::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstitutionPaymentRequestDetails()
    {
        return $this->hasMany(InstitutionPaymentRequestDetail::className(), ['loan_item_id' => 'loan_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProgrammeFees()
    {
        return $this->hasMany(ProgrammeFee::className(), ['loan_item_id' => 'loan_item_id']);
    }
}
