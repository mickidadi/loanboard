<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "m2k_invoice".
 *
 * @property integer $id
 * @property integer $Invoice_number
 * @property double $premium
 * @property integer $clientId
 * @property integer $premiumId
 * @property integer $branchId
 * @property integer $userId
 * @property string $date
 *
 * @property M2kStaff $user
 * @property M2kCustomer $client
 * @property M2kPremium $premium0
 * @property M2kBranch $branch
 */
class Invoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'm2k_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Invoice_number', 'premium', 'clientId', 'premiumId', 'userId'], 'required'],
            [['Invoice_number', 'clientId', 'premiumId', 'branchId', 'userId'], 'integer'],
            [['premium'], 'number'],
            [['date'], 'safe'],
            [['userId'], 'exist', 'skipOnError' => true, 'targetClass' => Staff::className(), 'targetAttribute' => ['userId' => 'm2k_csno']],
            [['clientId'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['clientId' => 'm2k_csno']],
            [['premiumId'], 'exist', 'skipOnError' => true, 'targetClass' => Premium::className(), 'targetAttribute' => ['premiumId' => 'id']],
            [['branchId'], 'exist', 'skipOnError' => true, 'targetClass' => Branch::className(), 'targetAttribute' => ['branchId' => 'branchId']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Invoice_number' => 'Invoice Number',
            'premium' => 'Premium',
            'clientId' => 'Client ID',
            'premiumId' => 'Premium ID',
            'branchId' => 'Branch ID',
            'userId' => 'User ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Staff::className(), ['m2k_csno' => 'userId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Customer::className(), ['m2k_csno' => 'clientId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPremium0()
    {
        return $this->hasOne(Premium::className(), ['id' => 'premiumId']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBranch()
    {
        return $this->hasOne(Branch::className(), ['branchId' => 'branchId']);
    }
}
