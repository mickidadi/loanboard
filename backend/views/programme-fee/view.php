<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\modules\allocation\models\ProgrammeFee */

$this->title = $model->programme_fee_id;
$this->params['breadcrumbs'][] = ['label' => 'Programme Fees', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="programme-fee-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->programme_fee_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->programme_fee_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'programme_fee_id',
            'academic_year_id',
            'programme_id',
            'loan_item_id',
            'amount',
            'days',
            'year_of_study',
        ],
    ]) ?>

</div>
