<?php

namespace frontend\modules\disbursement\controllers;

use yii\web\Controller;

/**
 * Default controller for the `disbursement` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout="main_disbursement";
        return $this->render('index');
    }
}
