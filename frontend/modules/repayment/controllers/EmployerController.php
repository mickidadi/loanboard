<?php

namespace frontend\modules\repayment\controllers;

use Yii;
use frontend\modules\repayment\models\Employer;
use frontend\modules\repayment\models\EmployerSearch;
use frontend\modules\application\models\User;
//use frontend\modules\application\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmployerController implements the CRUD actions for Employer model.
 */
class EmployerController extends Controller {

    /**
     * @inheritdoc
     */
    public $layout = "main_repayment";

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Employer models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new EmployerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Employer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    public function actionViewEmployerSuccess($id) {
        $this->layout = "main_home";
        return $this->render('viewEmployerSuccess', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Employer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    /*
      public function actionCreate()
      {
      $model = new Employer();

      if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->employer_id]);
      } else {
      return $this->render('create', [
      'model' => $model,
      ]);
      }
      }
     * 
     */
    public function actionCreate() {
        $this->layout = "main_public";
        $model1 = new Employer();
        $model2 = new User();
        $model2->scenario = 'employer_registration';
        $model1->scenario = 'employer_details';
        $model1->created_at = date("Y-m-d");
        $model2->created_at = date("Y-m-d H:i:s");
        $model2->last_login_date = date("Y-m-d H:i:s");
        if ($model1->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post())) {
            $model1->email_address = $model2->email_address;
            $model2->username = $model2->email_address;
            //$model1->employer_code='wsed215';
            $model1->physical_address = $model1->postal_address;
            $password = $model2->password1;
            $model2->password_hash = Yii::$app->security->generatePasswordHash($password);
            $model2->auth_key = Yii::$app->security->generateRandomString();
            $model2->status = 10;
            $model2->login_type = 2;
        }
        //if ($model1->load(Yii::$app->request->post()) && $model2->load(Yii::$app->request->post()) && $model1->save() && $model2->save()) {
        if ($model2->load(Yii::$app->request->post()) && $model2->save()) {
            $model1->user_id = $model2->user_id;
            if ($model1->load(Yii::$app->request->post()) && $model1->save()) {
                return $this->redirect(['view-employer-success', 'id' => $model1->employer_id]);
            }
        } else {
            return $this->render('create', [
                        'model1' => $model1, 'model2' => $model2,
            ]);
        }
    }

    /**
     * Updates an existing Employer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->employer_id]);
        } else {
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Employer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Employer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Employer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Employer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
