<?php

namespace frontend\modules\repayment\controllers;

use Yii;
use frontend\modules\repayment\models\EmployedBeneficiary;
use frontend\modules\repayment\models\EmployedBeneficiarySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * EmployedBeneficiaryController implements the CRUD actions for EmployedBeneficiary model.
 */
class EmployedBeneficiaryController extends Controller
{
    /**
     * @inheritdoc
     */
    
    public $layout="main_private";
    
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmployedBeneficiary models.
     * @return mixed
     */
    public function actionIndex()
    {
        $user_loged_in=Yii::$app->user->identity->login_type;
        if($user_loged_in==5){
           $this->layout="main_repayment"; 
        }else if($user_loged_in==2){
           $this->layout="main_repayment_employer"; 
        }
        $searchModel = new EmployedBeneficiarySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmployedBeneficiary model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $user_loged_in=Yii::$app->user->identity->login_type;
        if($user_loged_in==5){
           $this->layout="main_repayment"; 
        }else if($user_loged_in==2){
           $this->layout="main_repayment_employer"; 
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmployedBeneficiary model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $user_loged_in=Yii::$app->user->identity->login_type;
        if($user_loged_in==5){
           $this->layout="main_repayment"; 
        }else if($user_loged_in==2){
           $this->layout="main_repayment_employer"; 
        }
        $model = new EmployedBeneficiary();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->employed_beneficiary_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    /*
    public function actionImportExcel(){
        
        //$modelFile ->file = $firstName. '_' .$middleName. '_' .date('Y-m-d'). '_' .$file ->getBaseName(). "." .$file ->getExtension();
        //$inputFiles = 'uploads/beneficiaries_employed.xlsx';
        //$objPHPExcel = new \PHPExcel();
        $inputFiles = 'uploads/beneficiaries_employed.xlsx';

        try {
          $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
          $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader ->load($inputFiles);
        } catch (Exception $ex) {
          die('Error');
        }

        $sheet = $objPHPExcel ->getSheet(0);
        $highestRow = $sheet ->getHighestRow();
        $highestColumn = $sheet ->getHighestColumn();

        //$row is start 2 because first row assigned for heading.         
        for ($row = 3; $row <= $highestRow; ++$row) {

          $rowData = $sheet ->rangeToArray('A'.$row. ':' .$highestColumn.$row, NULL, TRUE, FALSE);

          //save to branch table.
          $modelHeader = new EmployedBeneficiary();
          //$modelDetail = new FakturOutDetail();

          //$modelHeader->employed_beneficiary_id = $rowData[0][0];
          $modelHeader->employer_id = "2";
          $modelHeader->employee_id = "1";
          $modelHeader->applicant_id = "1";
          //$modelHeader->basic_salary = "1000";
          $modelHeader->employment_status = "ONPOST";
          $modelHeader->created_on = date("Y-m-d H:i:s");
          $modelHeader->created_by = "2";         
          
          $modelHeader->employee_check_number = $rowData[0][1];
          $modelHeader->employee_f4indexno = $rowData[0][2];
          $modelHeader->employee_full_name = $rowData[0][3];
          $modelHeader->employee_mobile_phone_no = $rowData[0][4];
          $modelHeader->employee_current_nameifchanged = $rowData[0][5];
          $modelHeader->employee_year_completion_studies  = $rowData[0][6];
          $modelHeader->employee_academic_awarded = $rowData[0][7];
          $modelHeader->employee_instituitions_studies = $rowData[0][8];
          $modelHeader->employee_NIN = $rowData[0][9];
          $modelHeader->basic_salary = str_replace(",","",$rowData[0][10]);
          
          if($modelHeader->employee_current_nameifchanged==""){
              
          }
          
          $modelHeader->save();
          
          print_r($modelHeader->getErrors());
        }
        die("Okay");
    }
    */
	public function actionUploadGeneral()
        {
            $user_loged_in=Yii::$app->user->identity->login_type;
        if($user_loged_in==5){
           $this->layout="main_repayment"; 
        }else if($user_loged_in==2){
           $this->layout="main_repayment_employer"; 
        }
           $modelHeader = new EmployedBeneficiary();
		   $modelHeader->scenario = 'Uploding_employed_beneficiaries';
 
           if($modelHeader->load(Yii::$app->request->post()))
             {
                          $date_time=date("Y_m_d_H_i_s");
                          $inputFiles1=UploadedFile::getInstance($modelHeader, 'imageFile');
                          $modelHeader->imageFile=UploadedFile::getInstance($modelHeader, 'imageFile');
                          $modelHeader->upload($date_time);
                          $inputFiles = 'uploads/'.$date_time.$inputFiles1;
		
        try {
          $inputFileType = \PHPExcel_IOFactory::identify($inputFiles);
          $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
          $objPHPExcel = $objReader ->load($inputFiles);
        } catch (Exception $ex) {
          die('Error');
        }

        $sheet = $objPHPExcel ->getSheet(0);
        $highestRow = $sheet ->getHighestRow();
        $highestColumn = $sheet ->getHighestColumn();
 
       
        if(strcmp($highestColumn,"K")==0 && $highestRow >=3){        
                  
  $s1=1;
  
                  for ($row = 3; $row <= $highestRow; ++$row) {

          $rowData = $sheet ->rangeToArray('A'.$row. ':' .$highestColumn.$row, NULL, TRUE, FALSE);

          //save to branch table.
          $modelHeader = new EmployedBeneficiary();

          $modelHeader->employer_id = "2";
          $modelHeader->employee_id = "1";
          $modelHeader->applicant_id = "1";
          $modelHeader->employment_status = "ONPOST";
          $modelHeader->created_on = date("Y-m-d H:i:s");
          $modelHeader->created_by = "2";         
          
          $modelHeader->employee_check_number = $rowData[0][1];
          $modelHeader->employee_f4indexno = $rowData[0][2];
          $modelHeader->employee_full_name = $rowData[0][3];
          $modelHeader->employee_mobile_phone_no = $rowData[0][4];
          $modelHeader->employee_current_nameifchanged = $rowData[0][5];
          $modelHeader->employee_year_completion_studies  = $rowData[0][6];
          $modelHeader->employee_academic_awarded = $rowData[0][7];
          $modelHeader->employee_instituitions_studies = $rowData[0][8];
          $modelHeader->employee_NIN = $rowData[0][9];
          $modelHeader->basic_salary = str_replace(",","",$rowData[0][10]);
          $checkIsmoney=$modelHeader->basic_salary;
          if($modelHeader->validate()){
          if(!is_numeric($checkIsmoney)){
           $modelHeader->basic_salary="";
           $sms = '<p style="color: #cc0000">Operation failed, Please check excel colums.</p>';
                   Yii::$app->session->setFlash('sms', $sms);
          }
          
          $modelHeader->save();
                  }
        } 
        $sms = '<p>Information Successful Uploaded.</p>';

                   Yii::$app->session->setFlash('sms', $sms);
             }else{
               //$sms = '<p style="color: #cc0000">Operation failed, Please check excel colums.</p>';
               $sms = '<p>Operation failed, Please check excel colums.</p>';
                   Yii::$app->session->setFlash('sms', $sms);  
             }
                   

             }  
 
          return $this->render("upload_general", ['model'=>$modelHeader]);
        }
	
   /*
    public function actionUpload()
    {
        $model = new EmployedBeneficiary();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                //file is uploaded successfully
                //return;
            }
        }

        return $this->render('upload_23', ['model' => $model]);
    }
    * 
    */
      
     

    /**
     * Updates an existing EmployedBeneficiary model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $user_loged_in=Yii::$app->user->identity->login_type;
        if($user_loged_in==5){
           $this->layout="main_repayment"; 
        }else if($user_loged_in==2){
           $this->layout="main_repayment_employer"; 
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->employed_beneficiary_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    
    public function actionDownload()
{
    $path=Yii::getAlias('@webroot'). '/dwload';
    $file=$path. '/beneficiaries_employed.xlsx';
    if (file_exists($file)) {
        return Yii::$app->response->sendFile($file);
    } else {
        throw new \yii\web\NotFoundHttpException("{$file} is not found!");
    }
}

    /**
     * Deletes an existing EmployedBeneficiary model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmployedBeneficiary model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmployedBeneficiary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmployedBeneficiary::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
