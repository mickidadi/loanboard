<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\modules\repayment\models\EmployedBeneficiary */

$this->title = 'Update Employed Beneficiary: ' . $model->employed_beneficiary_id;
$this->params['breadcrumbs'][] = ['label' => 'Employed Beneficiaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->employed_beneficiary_id, 'url' => ['view', 'id' => $model->employed_beneficiary_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="employed-beneficiary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
