<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\modules\repayment\models\EmployedBeneficiarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employed Beneficiaries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employed-beneficiary-index">

<div class="panel panel-info">
                        <div class="panel-heading">
                      <?= Html::encode($this->title) ?>
                        </div>
                        <div class="panel-body">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Employee Loan Beneficiary', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Download Template', ['download'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Upload Employees Loan Beneficiaries', ['upload-general'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'employee_full_name',
            'employee_f4indexno',
            'employee_check_number',
            'employee_NIN',            
            'basic_salary',            
            /*
            'employed_beneficiary_id',
            'employer_id',
            'employee_id',
            'applicant_id',
            'basic_salary',
             * 
             */
            // 'employment_status',
            // 'created_on',
            // 'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
       </div>
</div>
