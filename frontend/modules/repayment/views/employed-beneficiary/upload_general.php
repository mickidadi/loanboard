<?php

use yii\helpers\Html;
?>    
    <?php if (Yii::$app->session->hasFlash('sms')): ?>
<div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <h4>
  <?= Yii::$app->session->getFlash('sms') ?>
      <div class="viewAll">
      <?= Html::a('<< BACK', ['index'], ['class' => 'btn btn-success']) ?>
      </div>
      </h4>
  </div>
<?php endif; ?>
   <?php 
/* @var $this yii\web\View */
/* @var $model frontend\modules\repayment\models\EmployedBeneficiary */

$this->title = 'Upload Employees Loan Beneficiaries';
$this->params['breadcrumbs'][] = ['label' => 'Upload Employees Loan Beneficiaries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employed-beneficiary-create">

<div class="panel panel-info">
        <div class="panel-heading">
       <?= Html::encode($this->title) ?>
        </div>
        <div class="panel-body">

    <?= $this->render('upload', [
        'model' => $model,
    ]) ?>

</div>
    </div>
</div>
