<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\modules\repayment\models\Employer */
$this->title = 'Employers Account Creation';
//$this->title = $model->employer_id;
//$this->params['breadcrumbs'][] = ['label' => 'Employers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employer-view">
    <div class="panel panel-info">
        <div class="panel-heading">
Congratulations!
<br/>
You have created an account on the HESLB, Please check your email address to activate your account.
        </div>
    </div>
</div>
