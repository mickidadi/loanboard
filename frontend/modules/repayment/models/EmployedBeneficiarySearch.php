<?php

namespace frontend\modules\repayment\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\modules\repayment\models\EmployedBeneficiary;

/**
 * EmployedBeneficiarySearch represents the model behind the search form about `frontend\modules\repayment\models\EmployedBeneficiary`.
 */
class EmployedBeneficiarySearch extends EmployedBeneficiary
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employed_beneficiary_id', 'employer_id', 'applicant_id', 'created_by'], 'integer'],
            [['employee_id', 'employment_status', 'created_on','employee_check_number','employee_f4indexno','employee_full_name','employee_mobile_phone_no',
                'employee_year_completion_studies','employee_academic_awarded','employee_instituitions_studies','employee_NIN','employee_check_number'], 'safe'],
            [['basic_salary'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmployedBeneficiary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'employed_beneficiary_id' => $this->employed_beneficiary_id,
            'employer_id' => $this->employer_id,
            'applicant_id' => $this->applicant_id,
            'basic_salary' => $this->basic_salary,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'employee_id', $this->employee_id])
            ->andFilterWhere(['like', 'employment_status', $this->employment_status])                
            ->andFilterWhere(['like', 'employee_check_number', $this->employee_check_number])
            ->andFilterWhere(['like', 'employee_f4indexno', $this->employee_f4indexno])
            ->andFilterWhere(['like', 'employee_full_name', $this->employee_full_name])
            ->andFilterWhere(['like', 'employee_mobile_phone_no', $this->employee_mobile_phone_no])
            ->andFilterWhere(['like', 'employee_year_completion_studies', $this->employee_year_completion_studies])
            ->andFilterWhere(['like', 'employee_academic_awarded', $this->employee_academic_awarded])
            ->andFilterWhere(['like', 'employee_instituitions_studies', $this->employee_instituitions_studies])
            ->andFilterWhere(['like', 'employee_NIN', $this->employee_NIN])
            ->andFilterWhere(['like', 'employee_check_number', $this->employee_check_number]);

        return $dataProvider;
    }
}
