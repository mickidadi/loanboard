<?php

namespace frontend\modules\repayment\models;

use Yii;

/**
 * This is the model class for table "employer".
 *
 * @property integer $employer_id
 * @property integer $user_id
 * @property string $employer_name
 * @property string $employer_code
 * @property string $employer_type
 * @property string $postal_address
 * @property string $phone_number
 * @property string $physical_address
 * @property integer $ward_id
 * @property string $email_address
 * @property integer $loan_repayment_bill_requested
 * @property string $created_at
 *
 * @property EmployedBeneficiary[] $employedBeneficiaries
 * @property User $user
 * @property Ward $ward
 * @property LoanRepaymentBatch[] $loanRepaymentBatches
 * @property LoanRepaymentBill[] $loanRepaymentBills
 */
class Employer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employer';
    }

    /**
     * @inheritdoc
     */
    public $district;
    public $employerCode;
    public function rules()
    {
        return [
            //[['user_id', 'employer_name', 'employer_code', 'phone_number', 'physical_address', 'email_address'], 'required'],
            [['user_id','employer_name', 'employer_code', 'phone_number','employer_type','postal_address','ward_id','district'], 'required', 'on'=>'employer_details'],
            [['user_id', 'ward_id', 'loan_repayment_bill_requested','district','phone_number'], 'integer'],
            [['employer_type','employerCode'], 'string'],
            [['created_at', 'email_address','employerCode'], 'safe'],
            [['employer_name', 'physical_address', 'email_address'], 'string', 'max' => 100],
            [['employer_code'], 'string', 'max' => 20],
            [['postal_address'], 'string', 'max' => 30],
            [['phone_number'], 'string', 'max' => 50],            
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => \frontend\modules\application\models\User::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['ward_id'], 'exist', 'skipOnError' => true, 'targetClass' => \backend\modules\application\models\Ward::className(), 'targetAttribute' => ['ward_id' => 'ward_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employer_id' => 'Employer ID',
            'user_id' => 'User ID',
            'employer_name' => 'Employer Name',
            'employer_code' => 'Employer Code',
            'employer_type' => 'Employer Type',
            'postal_address' => 'Postal Address',
            'phone_number' => 'Fixed Telesphone No.',
            'physical_address' => 'Physical Address',
            'ward_id' => 'Ward',
            'email_address' => 'Email Address',
            'loan_repayment_bill_requested' => 'Loan Repayment Bill Requested',
            'created_at' => 'Created At',
            'district'=>'District',
            'employerCode'=>'Employer Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployedBeneficiaries()
    {
        return $this->hasMany(EmployedBeneficiary::className(), ['employer_id' => 'employer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\frontend\modules\application\models\User::className(), ['user_id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWard()
    {
        return $this->hasOne(\backend\modules\application\models\Ward::className(), ['ward_id' => 'ward_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanRepaymentBatches()
    {
        return $this->hasMany(LoanRepaymentBatch::className(), ['employer_id' => 'employer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoanRepaymentBills()
    {
        return $this->hasMany(LoanRepaymentBill::className(), ['employer_id' => 'employer_id']);
    }
    //public function updatingUser($passedData){
            //$this->updateByPk($passedData,array('newOrder'=>'1'), new CDbCriteria(array('condition'=>'id = :id', 'params'=>array('id'=>$passedData))));
       // }
}
