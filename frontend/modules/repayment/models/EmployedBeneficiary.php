<?php

namespace frontend\modules\repayment\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "employed_beneficiary".
 *
 * @property integer $employed_beneficiary_id
 * @property integer $employer_id
 * @property string $employee_id
 * @property integer $applicant_id
 * @property double $basic_salary
 * @property string $employment_status
 * @property string $created_on
 * @property integer $created_by
 *
 * @property Applicant $applicant
 * @property Employer $employer
 * @property User $createdBy
 */
class EmployedBeneficiary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employed_beneficiary';
    }

    /**
     * @inheritdoc
     */
    public $imageFile;
	public $file;
        public $employee_check_number;
        public $employee_f4indexno;
        public $employee_full_name;
        public $employee_mobile_phone_no;
        public $employee_year_completion_studies;
        public $employee_academic_awarded;
        public $employee_instituitions_studies;
        public $employee_NIN;
        public $employee_current_nameifchanged;
    
    public function rules()
    {
        return [
            [['employee_check_number','employee_f4indexno', 'employee_full_name', 'employee_mobile_phone_no', 'employee_year_completion_studies',
                'employee_academic_awarded','employee_instituitions_studies','employee_NIN','basic_salary'], 'required', 'on'=>'Uploding_employed_beneficiaries'],
            [['employer_id', 'applicant_id', 'created_by'], 'integer'],
            [['basic_salary'], 'number'],
            [['employment_status'], 'string'],
            [['employer_id', 'employee_id', 'applicant_id', 'employment_status', 'created_on', 'created_by', 'created_on','employee_current_nameifchanged'], 'safe'],
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx,xls,csv','on'=>'Uploding_employed_beneficiaries'],
            [['file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'xlsx,xls','on'=>'uploaded_files_employees'],
            [['employee_id'], 'string', 'max' => 20],
            /*
			['file', 'file','on'=>'uploaded_files_aggregates',
                                            'types'=>'csv',
                                            'maxSize'=>1024 * 1024 * 10, // 10MB
                                            'tooLarge'=>'The file was larger than 10MB. Please upload a smaller file.',
                                            'allowEmpty' => false
                              ],
             * 
             */
            [['applicant_id'], 'exist', 'skipOnError' => true, 'targetClass' => \frontend\modules\application\models\Applicant::className(), 'targetAttribute' => ['applicant_id' => 'applicant_id']],
            [['employer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employer::className(), 'targetAttribute' => ['employer_id' => 'employer_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => \frontend\modules\application\models\User::className(), 'targetAttribute' => ['created_by' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employed_beneficiary_id' => 'Employed Beneficiary ID',
            'employer_id' => 'Employer ID',
            'employee_id' => 'Employee ID',
            'applicant_id' => 'Applicant ID',
            'basic_salary' => 'Basic Salary(TZS)',
            'employment_status' => 'Employment Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'employee_check_number'=>'employee_check_number',
            'employee_f4indexno'=>'Form IV Index No.',
            'employee_full_name'=>'Employee Name',
            'employee_mobile_phone_no'=>'Employee mobile phone No.',
            'employee_year_completion_studies'=>'Employee year of completion studies',
            'employee_academic_awarded'=>'Employee academic award',
            'employee_instituitions_studies'=>'Employee instituitions studies',
            'employee_NIN'=>'National Identification No.',
            //'basic_salary'=>'basic_salary',
            'employee_check_number'=>'Check Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicant()
    {
        return $this->hasOne(\frontend\modules\application\models\Applicant::className(), ['applicant_id' => 'applicant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployer()
    {
        return $this->hasOne(Employer::className(), ['employer_id' => 'employer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(\frontend\modules\application\models\User::className(), ['user_id' => 'created_by']);
    }
    
    
    public function upload($date_time)
    {
        if ($this->validate()) {
            //$date_time=date("Y_m_d_H_i_s");
            $this->imageFile->saveAs('uploads/' . $date_time.$this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            $this->imageFile->saveAs('uploads/' . $date_time.$this->imageFile->baseName . '.' . $this->imageFile->extension);
            return false;
        }
    }
     
     
}
