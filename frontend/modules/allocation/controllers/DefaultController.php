<?php

namespace frontend\modules\allocation\controllers;

use yii\web\Controller;

/**
 * Default controller for the `allocation` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
         $this->layout="main_allocation";
        return $this->render('index');
    }
}
